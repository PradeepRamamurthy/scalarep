package chapter4

class FirstExample {
  
  private val weight = 0.4
  
  def weightedMethod1(marks: Int): Double = {
    marks*weight
  }
  
  

}


object FirstExample{
  
  val obj1 = new FirstExample
  
  def callCompanion(): Unit = {
     println("weighted Method Call: " + obj1.weightedMethod1(13) +"  with weight = " + obj1.weight)
  }
}