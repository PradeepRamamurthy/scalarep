package chapter3
/*
 * Lists - used as an example of immutable sequence
 */
object Ex2_List extends App {
  
  val onetwothree = List(1,2,3)
  val threefour = List(3,4)
  
  //Concatenation operator in List called ':::'
  
  println(onetwothree:::threefour)
  println("onetwothree= " + onetwothree)
  println("threefour= " + threefour)
  
  //Cons operator '::'
  
  println(7 :: onetwothree)
 
  println("onetwothree= " + onetwothree)
  //Creating a new List using the cons operator and Nil
  
 val a1 = 1::2::3::4::Nil


 
  
}