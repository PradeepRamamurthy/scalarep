package chapter3
/*
 * Example showing conversion of a while loop from
 * from Imperative to functional style by getting
 * rid of vars.
 */
object Ex6_ImperativeToFunctional extends App {

  //Define an Array of Strings
  val args1 = Array("Hello", "this", "is", "a", "beautiful", "World")

  //imperative style
  exampleprintArgs(args1)
  println("*****************")
  example1printArgs(args1)
  println("*****************")
  example2printArgs(args1)
  println("*****************")
  println(example3Args(args1))

  def exampleprintArgs(argsdfksdf: Array[String]): Unit = {

    var i = 0
    while (i < argsdfksdf.length) {
      println(argsdfksdf(i))
      i += 1

    }

  }

  //A little more functional style by getting rid of vars using for loop

  def example1printArgs(argsdfksdf: Array[String]): Unit = {
    for (arg <- args1) {
      println(arg)
    }
  }

  //A little more functional style by getting rid of vars using foreach loop
  def example2printArgs(argsdfksdf: Array[String]): Unit = {
    args1.foreach(println)
  }
  
  def example3Args(argsdfksdf: Array[String]): String = {
    
    argsdfksdf.mkString(",")

  }

}