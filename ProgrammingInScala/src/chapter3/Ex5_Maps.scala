package chapter3
import scala.collection.mutable.Map
/*
 * Example of Map Creation
 */
object Ex5_Maps extends App {
  
  val treasureMap = Map[Int,String]()
  treasureMap += (1 -> "Go to Island")
  treasureMap += (2 -> "Find an Egg rock")
  treasureMap += (3 -> "Find  Post")
  
  println(treasureMap(2))
  
  
}

