package chapter3
import scala.collection.mutable
/*
 * Set creation example.
 */
object Ex4_Sets extends App {
  
  //immutable version of Set
   val set1 = Set("Boeing","Airbus")
   val set2 = set1 + "Lear"
   
   println(set1 == set2)
   println(set1)
  
  //Mutable version of Set
  val set3 = mutable.Set("Boeing","Airbus")
  val set4 = set3 += "Jag"
  println(set3 == set4)
  println(set4)
  
  
}
