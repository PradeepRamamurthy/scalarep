package chapter3
//This is an example of Parameterizing an Array with a type and another example showing operator methods
object ParameterizingAnArrayWithAType extends App {

  val greetString = new Array[String](5)

  //println(greetString.length)
  greetString(0) = "17"
  greetString(1) = "Doodleway"
  greetString(2) = "-"
  greetString(3) = "Melbourne"
  greetString(4) = "Venkat"

  for (i <- 0 to 4) {
    println(greetString(i))
  }

  println("----------------")
  //operators are methods
  val op1 = 2 + 3
  val op2 = 2.+(3)
  println(op1)
  println(op2)
}