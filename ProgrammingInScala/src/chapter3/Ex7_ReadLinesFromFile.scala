package chapter3
import scala.io.Source

//Example showing reading lines from a text file
object Ex7_ReadLinesFromFile extends App {
  var filePath = "src/resources/sampletext1.txt"
  
  val fileobj = Source.fromFile(filePath)
  for(line <- fileobj.getLines()){
    println(line.length() +" "+"|"+" "+ line)
    
  }
}

