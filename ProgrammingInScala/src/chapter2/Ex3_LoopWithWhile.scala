package chapter2
/*
 * Example of while loop in Scala
 */
object LoopWithWhile extends App {

  //An imperative style of looping

  var i = 0;
  val sampleArray = Array("Apples", "Oranges", "Monkeys", "Humans")
  
//Printing on seperate lines
  while (i < sampleArray.length) {
    println(sampleArray(i))
    i += 1
  }

  //printing on same line
  var j = 0
  while (j < sampleArray.length) {
    if (j != 0) {
      print(" ")
    }
    print(sampleArray(j))
    j += 1

  }
  println()

}