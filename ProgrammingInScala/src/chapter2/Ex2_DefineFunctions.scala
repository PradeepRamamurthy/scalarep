package chapter2
/*
 * Example of how to define functions in Scala.
 * Function definitions start with def.
 */


object Ex2_DefineFunctions extends App {

  //function max takes two Int parameters and returns an Int
  def max(x: Int, y: Int): Int = {
    if (x > y) x else y
  }

  println("Max: "+max(55, 51))

  //function greet() takes no parameter and returns no interesting value.(result/return type of Unit)

  def greet(): Unit = println("This is a Wonderful Hello World!!")

  greet()

}