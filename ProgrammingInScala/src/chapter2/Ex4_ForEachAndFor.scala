package chapter2
/*
 * This example shows simple usage of foreach and for loops in Scala
 */
object Ex4_ForEachAndFor extends App {

  //This is more functional style 

  val bookNames = Array("Moby Dick", "War and Peace", "Don Quixote", "Odyssey")

  bookNames.foreach(book => println(book))
  //more explicitly
  //bookNames.foreach((book: String) => println(book))

  println("----------------")
  
  //for expression example - you can read it as for 'book in bookNames'
  //On function relative of for loop as shown below is available in Scala.
  for (book <- bookNames) {
    println(book)
  }

}