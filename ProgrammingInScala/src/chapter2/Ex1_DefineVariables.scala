package chapter2
/*
 * Scala has two types of variables vals and vars.
 * A val is similar to a final variable in Java and once assigned
 * can never be reassigned.
 * Var however can be reassigned throughout its lifetime.
 * 
 */

object Ex1_DefineVariables extends App {

  //define a val and assign a value to it.
  val msg = "Hello World"
  println(msg)

  //val cannot be reassigned - it will throw a compiler error.
  //msg = "Change the World"

  //Define a var and assign a value to it.
  var count = 5
  //reassign a var.
  count = count + 3
  println(count)

}